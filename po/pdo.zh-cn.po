#
# Yangfl <mmyangfl@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: pdo\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-11 15:50+0800\n"
"PO-Revision-Date: 2017-09-25 21:24+0800\n"
"Last-Translator: Yangfl <mmyangfl@gmail.com>\n"
"Language-Team:  <debian-l10n-chinese@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: bin/create_index_pages:69
msgid "virtual package provided by"
msgstr "本虚包由这些包填实:"

#: bin/create_index_pages:171 bin/create_index_pages:212
msgid "Section"
msgstr "版面"

#: bin/create_index_pages:178 bin/create_index_pages:220
msgid "Subsection"
msgstr "子版面"

#: bin/create_index_pages:185 bin/create_index_pages:228
msgid "Priority"
msgstr "优先级"

#: lib/Packages/Dispatcher.pm:347
msgid "requested format not available for this document"
msgstr "要求的格式在此文档中暂时不可用"

#: lib/Packages/DoDownload.pm:27 lib/Packages/DoFilelist.pm:27
#: lib/Packages/DoShow.pm:31
msgid "package not valid or not specified"
msgstr "软件包无效或未指定"

#: lib/Packages/DoDownload.pm:30 lib/Packages/DoFilelist.pm:30
#: lib/Packages/DoIndex.pm:37 lib/Packages/DoNewPkg.pm:22
#: lib/Packages/DoSearchContents.pm:30 lib/Packages/DoShow.pm:34
msgid "suite not valid or not specified"
msgstr "发行版不合法或未指定"

#: lib/Packages/DoDownload.pm:33 lib/Packages/DoFilelist.pm:33
msgid "architecture not valid or not specified"
msgstr "硬件架构不合法或未指定"

#: lib/Packages/DoDownload.pm:36
#, perl-format
msgid "more than one suite specified for download (%s)"
msgstr "不止一套发行版被指定下载(%s)"

#: lib/Packages/DoDownload.pm:40
#, perl-format
msgid "more than one architecture specified for download (%s)"
msgstr "不止一种硬件架构被指定下载(%s)"

#: lib/Packages/DoDownload.pm:63 lib/Packages/DoShow.pm:73
msgid "No such package."
msgstr "没有这样的软件包。"

#: lib/Packages/DoDownload.pm:90
msgid "kByte"
msgstr "kB"

#: lib/Packages/DoDownload.pm:93
msgid "MByte"
msgstr "MB"

#: lib/Packages/DoFilelist.pm:48
msgid "No such package in this suite on this architecture."
msgstr "在此发行版中这种硬件架构下缺少这样的软件包"

#: lib/Packages/DoFilelist.pm:60
msgid "Invalid suite/architecture combination"
msgstr "无效的发行版/硬件架构组合"

#: lib/Packages/DoIndex.pm:40
#, perl-format
msgid "more than one suite specified for show_static (%s)"
msgstr "不止一套发行版被指定 show_static(%s)"

#: lib/Packages/DoIndex.pm:44
#, perl-format
msgid "more than one subsection specified for show_static (%s)"
msgstr "不止一套子版面被指定 show_static(%s)"

#: lib/Packages/DoIndex.pm:81
#, perl-format
msgid "couldn't read index file %s: %s"
msgstr "未能读取索引文件 %s: %s"

#: lib/Packages/DoNewPkg.pm:25
#, perl-format
msgid "more than one suite specified for newpkg (%s)"
msgstr "不止一套发行版被指定显示(%s)"

#: lib/Packages/DoNewPkg.pm:43
#, perl-format
msgid "no newpkg information found for suite %s"
msgstr "没能在发行版 %s 中找到相关信息"

#: lib/Packages/DoSearch.pm:25 lib/Packages/DoSearchContents.pm:24
msgid "keyword not valid or missing"
msgstr "关键字不合法或不存在"

#: lib/Packages/DoSearch.pm:28 lib/Packages/DoSearchContents.pm:27
msgid "keyword too short (keywords need to have at least two characters)"
msgstr "关键字太短(关键字需要至少包含两个字符)"

#: lib/Packages/DoSearch.pm:169
msgid "Exact hits"
msgstr "完整匹配"

#: lib/Packages/DoSearch.pm:179
msgid "Other hits"
msgstr "部分匹配"

#: lib/Packages/DoSearch.pm:238
msgid "Virtual package"
msgstr "虚包"

#: lib/Packages/DoSearchContents.pm:40
#, perl-format
msgid "more than one suite specified for contents search (%s)"
msgstr "不止一套发行版被指定要求搜索其内容(%s)"

#: lib/Packages/DoSearchContents.pm:62
msgid "No contents information available for this suite"
msgstr "内容信息在当前发行版中暂时不可用。"

#: lib/Packages/DoSearchContents.pm:86
msgid "The search mode you selected doesn't support more than one keyword."
msgstr "您选择的搜索模式不支持多个关键字。"

#: lib/Packages/DoShow.pm:37
#, perl-format
msgid "more than one suite specified for show (%s)"
msgstr "不止一套发行版被指定显示(%s)"

#: lib/Packages/DoShow.pm:85
msgid "Package not available in this suite."
msgstr "软件包在当前发行版中暂时不可用。"

#: lib/Packages/DoShow.pm:198
msgid " and others"
msgstr " 以及其他的"

#: lib/Packages/DoShow.pm:254
msgid "virtual package"
msgstr "虚包"

#: lib/Packages/DoShow.pm:435
#, perl-format
msgid "not %s"
msgstr "缺 %s"

#: lib/Packages/DoShow.pm:485
msgid "Package not available"
msgstr "软件包暂时不可用"

#: lib/Packages/DoShow.pm:524
msgid "Not available"
msgstr "暂时不可用"

#: lib/Packages/Page.pm:47
msgid "package has bad maintainer field"
msgstr "软件包存在错误的维护者字段"
